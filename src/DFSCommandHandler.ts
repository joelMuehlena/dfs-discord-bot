import { ApplicationCommandData, Client, CommandInteraction } from "discord.js";
import { DFSBot } from "./DFSBot";

export type DFSCommand = {
  command: ApplicationCommandData;
  guilds: string[];
  handler: (interaction: CommandInteraction) => void;
};

export class DFSCommandHandler {
  private commands_: Array<DFSCommand> = [];
  private client_: DFSBot;

  constructor(client: DFSBot) {
    this.client_ = client;
    this.pushAllCommands();
  }

  private async pushAllCommands() {
    if (this.commands_.length <= 0 || !this.client_.getReady()) return;

    console.log("[LOG] Update commands. Pushing...");

    this.commands_.forEach(async (command) => {
      if (command.guilds.length <= 0) {
        await this.client_.application?.commands.create(command.command);
      } else {
        command.guilds.forEach(async (guild) => {
          await this.client_.guilds.cache
            .get(guild)
            ?.commands.create(command.command);
        });
      }
    });
  }

  private async pushNewestCommand() {
    if (!this.client_.getReady()) return;

    console.log("[LOG] Update newest command. Pushing...");

    const lastCommand = this.commands_[this.commands_.length - 1];
    if (lastCommand.guilds.length <= 0) {
      await this.client_.application?.commands.create(lastCommand.command);
    } else {
      lastCommand.guilds.forEach(async (guild) => {
        await this.client_.guilds.cache
          .get(guild)
          ?.commands.create(lastCommand.command);
      });
    }
  }

  public update() {
    this.pushAllCommands();
  }

  public handle(interaction: CommandInteraction) {
    if (
      !interaction.isCommand ||
      this.commands_ === undefined ||
      this.commands_.length <= 0
    )
      return;

    const index = this.commands_.findIndex(
      (item) => item.command.name === interaction.commandName
    );
    if (index === -1) {
      console.log("[WARNING] unknown command " + interaction.commandName);
      interaction.reply("This command doesn't exist!", {
        ephemeral: true,
      });
    }

    this.commands_[index].handler(interaction);
  }

  public async register(command: DFSCommand) {
    this.commands_.push(command);
    await this.pushNewestCommand();
  }
}
