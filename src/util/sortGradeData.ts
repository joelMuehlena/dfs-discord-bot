import { GradeData } from "../GradeData.interface";

type retData = {
  [key: string]: GradeData[];
};

export const sortData = (data: GradeData[]): retData => {
  data.sort((a, b) => a.semester - b.semester);
  const moduleNamesArray = data.map((item) => item.modulName);
  const moduleNameSet = new Set(moduleNamesArray);

  const newObj: retData = {};

  moduleNameSet.forEach((item) => {
    newObj[item] = [];
  });

  data.forEach((item) => {
    newObj[item.modulName] = [...newObj[item.modulName], { ...item }];
  });

  return newObj;
};
