import dotenv from "dotenv";
import { format } from "mysql";

import {
  Intents,
  CommandInteraction,
  Message,
  User,
  MessageReaction,
  MessageEmbed,
  GuildChannel,
  TextChannel,
} from "discord.js";
import { DFSBot } from "./DFSBot";
import { MySQLConnection } from "./config/MySQLConnection";
import { iconAsNumber, numberAsIcon } from "./util/iconParser";
import { wait } from "./util/wait";
import { sortData } from "./util/sortGradeData";
import { GradeData } from "./GradeData.interface";

dotenv.config();

const client = new DFSBot({
  intents: [
    Intents.FLAGS.GUILDS,
    Intents.FLAGS.GUILD_MESSAGES,
    Intents.FLAGS.GUILD_MESSAGE_REACTIONS,
  ],
});
client.login(process.env.BOTTOKEN);

client.register({
  command: {
    name: "whois",
    description:
      "Fetch a Person by a Matrikelnummer or by a substring of a name",
    options: [
      {
        name: "input",
        type: "STRING",
        description: "Substring of name or a full Matrikelnummer",
        required: true,
      },
    ],
  },
  guilds: ["249047083519049730", "763404240244178976"],
  handler: async (interaction: CommandInteraction) => {
    const val = interaction.options[0].value;

    const num = Number(val);

    let findString = "";
    let found;
    if (isNaN(num) && val != undefined) {
      const sqlString = "SELECT * FROM dfsbot_persons WHERE name LIKE ?";
      const sqlInsert = [`%${val}%`];
      const { results } = await MySQLConnection.getInstance().query<{
        id: number;
        name: string;
        matNr: number;
      }>(format(sqlString, sqlInsert));

      found = results.length <= 0 ? false : true;
      findString += `There are *${results.length}* Persons matching this part of a name \n`;

      results.forEach((item) => {
        findString += `Name: **${item.name}** Number: **${item.matNr}** \n`;
      });
    } else {
      const sqlString = "SELECT * FROM dfsbot_persons WHERE matNr=?";
      const sqlInsert = [num];
      const { results } = await MySQLConnection.getInstance().query<{
        id: number;
        name: string;
        matNr: number;
      }>(format(sqlString, sqlInsert));

      found = results.length <= 0 ? false : true;
      findString = `You requested the Person: **${results[0].name}** with the number: **${results[0].matNr}**`;
    }

    await interaction.reply(!found ? "Nothing found" : findString, {
      ephemeral: false,
    });
  },
});

client.register({
  command: {
    name: "grade",
    description:
      "Fetch a Person by a Matrikelnummer or by a substring of a name",
    options: [
      {
        name: "input",
        type: "STRING",
        description: "Substring of name or a full Matrikelnummer",
        required: true,
      },
    ],
  },
  guilds: ["249047083519049730", "763404240244178976"],
  handler: async (interaction: CommandInteraction) => {
    const nameOrMat = interaction.options[0].value;
    const matNr = Number(nameOrMat);

    let sqlQueryString = "";
    if (isNaN(matNr) && nameOrMat != undefined) {
      const sqlString = "SELECT * FROM dfsbot_persons WHERE name LIKE ?";
      const sqlInsert = [`%${nameOrMat}%`];

      sqlQueryString = format(sqlString, sqlInsert);
    } else {
      const sqlString = "SELECT * FROM dfsbot_persons WHERE matNr=?";
      const sqlInsert = [matNr];
      sqlQueryString = format(sqlString, sqlInsert);
    }

    const { results } = await MySQLConnection.getInstance().query<{
      id: number;
      name: string;
      matNr: number;
    }>(sqlQueryString);

    if (results.length <= 0) {
      await interaction.reply("Nothing found", {
        ephemeral: true,
      });

      return;
    }

    let userMatNr;

    if (results.length > 1 && results.length <= 9) {
      let userString = "\n";
      results.forEach((person, index) => {
        userString += `${index + 1} Name: **${person.name}** Number: **${
          person.matNr
        }** \n`;
      });

      userString += "\nPlease select one via reaction";

      await interaction.reply(userString, {
        ephemeral: false,
      });

      const msg: Message = await interaction.fetchReply();

      const filterArray: string[] = [];
      for (let i = 0; i < results.length; ++i) {
        const icon = numberAsIcon(i + 1);
        await msg.react(icon);
        filterArray.push(icon);
      }

      const filter = (reaction: MessageReaction, user: User) => {
        return (
          filterArray.includes(reaction.emoji.name!) &&
          user.id === interaction.user.id
        );
      };

      try {
        const data = await msg.awaitReactions(filter, {
          time: 15000,
          max: 1,
          errors: ["time"],
        });

        userMatNr = results[iconAsNumber(data.first()?.emoji.name!)].matNr;
      } catch (err) {
        await interaction.editReply("You responded with the wrong reaction");
        await msg.reactions.removeAll();
        await wait(10 * 1000);
        interaction.deleteReply();
        return;
      }
    } else if (results.length > 9) {
      await interaction.reply(
        "Too many users please be more specific in your request",
        {
          ephemeral: true,
        }
      );
      return;
    }

    const sqlString =
      "SELECT dfsbot_persons.matNr, dfsbot_persons.name AS studentName, dfsbot_noten.points, dfsbot_noten.grade, dfsbot_noten.description, dfsbot_noten.totalPoints, dfsbot_module.modulName, dfsbot_module.semester, dfsbot_dozenten.name AS dozentName FROM (((dfsbot_persons INNER JOIN dfsbot_noten ON dfsbot_persons.matNr=dfsbot_noten.matNr) INNER JOIN dfsbot_module ON dfsbot_noten.modul=dfsbot_module.id) INNER JOIN dfsbot_dozenten ON dfsbot_module.dozent=dfsbot_dozenten.id) WHERE dfsbot_persons.matNr=? ORDER BY dfsbot_noten.description";
    const inserts = [results.length === 1 ? results[0].matNr : userMatNr];

    const dataQuery = await MySQLConnection.getInstance().query<GradeData>(
      format(sqlString, inserts)
    );

    if (dataQuery.results.length > 0) {
      const sortetAfterModules = sortData(dataQuery.results);

      const gradeEmbed = new MessageEmbed()
        .setColor("#0099ff")
        .setTitle(`Grades of ${dataQuery.results[0].studentName}`);

      Object.entries(sortetAfterModules).forEach(([key, value]) => {
        let gradeValues = "";
        value.forEach((gradeDataObject) => {
          gradeValues += `${gradeDataObject.description} - **${
            gradeDataObject.grade ? `Grade: ${gradeDataObject.grade}.` : ""
          } With ${gradeDataObject.points} Points${
            gradeDataObject.totalPoints > 0
              ? ` of ${gradeDataObject.totalPoints}.`
              : "."
          }**.\n`;
        });
        gradeEmbed.addField(
          `${key} - *Semester ${value[0].semester}*`,
          gradeValues
        );
      });

      gradeEmbed.setTimestamp();

      if (interaction.replied) {
        await interaction.editReply("", { embeds: [gradeEmbed] });
      } else {
        await interaction.reply({
          embeds: [gradeEmbed],
        });
      }
    } else {
      if (interaction.replied) {
        await interaction.deleteReply();
        await interaction.followUp("Nothing found", {
          ephemeral: true,
        });
      } else {
        await interaction.reply("Nothing found", { ephemeral: true });
      }
    }
  },
});

client.register({
  command: {
    name: "cite",
    description: "Get a random cite",
    options: [
      {
        name: "input",
        type: "STRING",
        description: "Substring of who said this?",
        required: false,
      },
    ],
  },
  guilds: ["249047083519049730", "763404240244178976"],
  handler: async (interaction: CommandInteraction) => {
    const citeChannel: GuildChannel | undefined = client.guilds.cache
      .get(interaction.guildID!)
      ?.channels.cache.find((channel) => channel.name == "zitate");

    if (
      !citeChannel ||
      citeChannel === undefined ||
      citeChannel.type != "text"
    ) {
      await interaction.reply("There is no zitate channel I'm sorry :(", {
        ephemeral: true,
      });
      return;
    }

    const messagesCollection = await (
      citeChannel as TextChannel
    ).messages.fetch();
    let messages = messagesCollection.array();

    if (messages.length <= 0) {
      await interaction.reply("There are no cites in the zitate channel :(", {
        ephemeral: true,
      });
      return;
    }

    if (interaction.options[0]?.value !== undefined) {
      messages = messages.filter((msg) =>
        msg.content.startsWith(interaction.options[0].value as string)
      );

      if (messages.length <= 0) {
        await interaction.reply(
          "There are no cites of this person in the zitate channel :(",
          {
            ephemeral: true,
          }
        );
        return;
      }
    }

    const rand = Math.floor(Math.random() * messages.length);

    await interaction.reply(messages[rand].content);
  },
});
