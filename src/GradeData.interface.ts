export type GradeData = {
  matNr: number;
  studentName: string;
  grade?: number;
  points: number;
  description: string;
  modulName: string;
  semester: number;
  dozentName: string;
  totalPoints: number;
};
