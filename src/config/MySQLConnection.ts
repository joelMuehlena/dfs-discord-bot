import { createPool, format, Pool } from "mysql";

export class MySQLConnection {
  private static instance: MySQLConnection;
  private connection: Pool;

  private constructor() {
    this.connection = createPool({
      user: process.env.DBUSER,
      password: process.env.DBPASS,
      host: process.env.DBHOST,
      database: process.env.DB,
    });
  }

  public static getInstance() {
    if (!MySQLConnection.instance) {
      MySQLConnection.instance = new MySQLConnection();
    }

    return MySQLConnection.instance;
  }

  public query<T = void>(
    sqlString: string
  ): Promise<{ results: T[]; fields: any }> {
    return new Promise((resolve, reject) => {
      this.connection.query(sqlString, (error, results, fields) => {
        if (error) {
          reject(error);
          return;
        }

        resolve({ results, fields });
      });
    });
  }
}
