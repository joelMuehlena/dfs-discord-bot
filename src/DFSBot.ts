import { Client, ClientOptions, CommandInteraction } from "discord.js";
import { DFSCommandHandler, DFSCommand } from "./DFSCommandHandler";

export class DFSBot extends Client {
  private commandHandler_: DFSCommandHandler;
  private isReady_: boolean = false;

  constructor(options: ClientOptions) {
    super(options);
    this.commandHandler_ = new DFSCommandHandler(this);
    super.addListener("interaction", this.interactionHandler);

    this.on("ready", this.readyHandler);
  }

  private readyHandler() {
    console.log("BOT loaded and ready");
    this.setReady(true);
    this.commandHandler_.update();
  }

  private interactionHandler(interaction: CommandInteraction) {
    this.commandHandler_.handle(interaction);
  }

  public setReady(ready: boolean) {
    this.isReady_ = ready;
  }

  public getReady(): boolean {
    return this.isReady_;
  }

  public register(command: DFSCommand) {
    this.commandHandler_.register(command);
  }
}
