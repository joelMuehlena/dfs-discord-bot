-- Should be used with MySQL

CREATE TABLE IF NOT EXISTS dfsbot_dozenten
(
    id   int unsigned auto_increment
        primary key,
    name varchar(300) not null,
    constraint name
        unique (name)
)
    collate = utf8_bin;

CREATE TABLE IF NOT EXISTS dfsbot_module
(
    id        int unsigned auto_increment
        primary key,
    modulName varchar(300) not null,
    semester  int          not null,
    dozent    int          not null,
    constraint modulName
        unique (modulName)
)
    collate = utf8_bin;

CREATE TABLE IF NOT EXISTS dfsbot_noten
(
    id          int auto_increment
        primary key,
    matNr       int          not null,
    modul       int          not null,
    description varchar(500) not null,
    points      float        not null,
    grade       float        null,
    totalPoints int          not null
)
    collate = utf8_bin;

CREATE TABLE IF NOT EXISTS dfsbot_persons
(
    id       int auto_increment
        primary key,
    name     varchar(255) not null,
    matNr    int unsigned not null,
    username varchar(50)  null,
    constraint matNr
        unique (matNr)
)
    collate = utf8_bin;

