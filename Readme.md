# DFS Discord Bot (der aller echte)

## How to use

Der Bot-Token muss als `BOTTOKEN=<your-token>` als Umgebungsvariable mitgegeben werden.

Für die MySQL funktionalität müssen die folgenden Umgebungsvariablen gesetzt werden:

- DBUSER=\<db-username\>
- DBPASS=\<db-password\>
- DBHOST=\<db-host\>
- DB=\<the-database-name\>
