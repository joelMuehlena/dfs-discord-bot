FROM node:15-alpine3.11

RUN apk add git

WORKDIR /bot

COPY "package.json" "package.json"

RUN yarn install --production

COPY "./build" "./build"

CMD ["node", "build/index.js"]